<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Auth::routes(['verify' => true]);

Route::get('/home', 'HomeController@index')->name('home')->middleware('verified');

Route::get('/posts', 'PostController@index')->name('posts.index');
Route::get('/posts/search', 'PostController@search')->name('posts.search');
Route::get('/posts/create', 'PostController@create')->name('posts.create')->middleware('auth');
Route::post('/posts', 'PostController@store')->name('posts.store')->middleware('auth');
Route::get('/posts/{post}/edit', 'PostController@edit')->name('posts.edit')->middleware('can:update-post,post');
Route::put('/posts/{post}', 'PostController@update')->name('posts.update')->middleware('can:update-post,post');
Route::get('/posts/{post}', 'PostController@show')->name('posts.show');
Route::delete('/posts/{post}', 'PostController@destroy')->name('posts.destroy')->middleware('can:update-post,post');




Route::get('/admin/users', 'Admin\UsersController@index')->middleware('verified', 'can:admin-users')->name('admin.users.index');
Route::get('/admin/users/search', 'Admin\UsersController@search')->middleware('verified', 'can:admin-users')->name('admin.users.search');
Route::get('/admin/users/{user}/edit', 'Admin\UsersController@edit')->middleware('verified', 'can:edit-users')->name('admin.users.edit');
Route::put('/admin/users/{user}','Admin\UsersController@update')->middleware('verified', 'can:edit-users')->name('admin.users.update');
Route::delete('/admin/users/{user}','Admin\UsersController@destroy')->middleware('verified', 'can:admin-users')->name('admin.users.destroy');

//Namespace admin = Folder z kontrolerem, prefix admin = w url,
// Route::namespace('Admin')->prefix('admin')->name('admin.')->middleware('verified', 'can:admin-users')->group(function(){
//   Route::resource('/users','UsersController', ['except' => ['show', 'create', 'store','update']]);
// });
