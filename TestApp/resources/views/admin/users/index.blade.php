@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Users</div>
                <div class="card-body">


                    <form action="/admin/users/search" method="get">

                      <div class="input-group mb-3">
                        <div class="input-group-prepend">
                          <span class="input-group-text" id="inputGroup-sizing-default">Name</span>
                        </div>
                        <input type="search" name="search_name" class="form-control" >
                      </div>
                      <div class="input-group mb-3">
                        <div class="input-group-prepend">
                          <span class="input-group-text" id="inputGroup-sizing-default">Email</span>
                        </div>
                        <input type="search" name="search_email" class="form-control" >
                      </div>
                      <span class="input-group-prepend">
                        <button type="submit" class="btn btn-primary form-control">Search</button>
                      </span>
                    </form>


                  <table class="table">
                    <thead>
                      <tr>
                        <th scope="col">Avatar</th>
                        <th scope="col">Name</th>
                        <th scope="col">Email</th>
                        <th scope="col">Roles</th>
                        <th scope="col">Actions</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach ($users as  $user)
                        <tr>
                          <td scope="row"><img src="/storage/{{ $user->avatar }}" class="rounded-circle" style="height: 50px; width: 50px;"/></td>
                          <td>{{ $user->name }}</td>
                          <td>{{ $user->email }}</td>
                          <td>{{ implode(', ' ,$user->roles()->get()->pluck('name')->toArray()) }}</td>
                          <td>
                            <a href="{{ route('admin.users.edit',  $user->id) }}"><button type="button" class="btn btn-primary float-left">Edit</button></a>
                            <form action="{{ route('admin.users.destroy', $user)}}" method="post" class="float-left">
                              @csrf
                              {{ method_field('delete')}}
                              <button type="submit" class="btn btn-warning">Delete</button>
                            </form>
                          </td>
                        </tr>
                      @endforeach
                    </tbody>
                  </table>

                  <div class="row">
                    <div class="col-12 d-flex justify-content-center">
                      {{ $users->links() }}
                    </div>
                  </div>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection
