@extends('layouts.app')

@section('content')
  <div class="container">
    <h1>New Post</h1>
    <hr />
    <form method="post" action="{{ route('posts.store') }}">
      {{ csrf_field() }}
      <div class="form-group">
        <label for="title">Title</label>
        <input type="text" class="form-control" id="title" name="title" placeholder="Title" value="{{ old('title') }}" required>
      </div>

      <div class="form-group">
        <label for="content">Post description</label>
        <textarea class="form-control" rows="8" id="content" name="content" placeholder="Write something amazing..." value="{{ old('content') }}" required></textarea>
      </div>

      <button type="submit" class="btn btn-primary btn-lg">Save Post</button>
    </form>

  </div>
@endsection
