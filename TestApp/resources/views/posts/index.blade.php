@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Users</div>
                <div class="card-body">

                  <a href="{{ route('posts.create') }}" class="btn btn-primary form-control" style="margin-top:15px;">Create New Post</a>

                  <hr />


                    <form action="/posts/search" method="get">
                      <div class="input-group mb-3">
                        <div class="input-group-prepend">
                          <span class="input-group-text" id="inputGroup-sizing-default">Title</span>
                        </div>
                        <input type="search" name="search_title" class="form-control" >
                      </div>
                      <span class="input-group-prepend">
                        <button type="submit" class="btn btn-primary form-control">Search</button>
                      </span>
                    </form>


                  <table class="table">
                    <thead>
                      <tr>
                        <th scope="col">Author</th>
                        <th scope="col">Post</th>
                        <th scope="col">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach ($posts as  $post)
                        <tr>
                            <td>{{ $post->user->name }}</td>
                            <td>{{ $post->title }}</td>
                            <td>
                              <a href="{{ route('posts.show',  $post->id) }}"><button type="button" class="btn btn-primary float-left">Show</button></a>

                              @can('update-post',$post->id)
                                <a href="{{ route('posts.edit', $post->id) }}"><button type="button" class="btn btn-warning float-left">Edit</button></a>
                              @endcan

                              @can('update-post',$post->id)
                                <form action="{{ route('posts.destroy', $post->id)}}" method="post" class="float-left">
                                  @csrf
                                  {{ method_field('delete')}}
                                  <button type="submit" class="btn btn-danger">Delete</button>
                                </form>
                              @endcan

                            </td>
                        </tr>
                      @endforeach
                    </tbody>
                  </table>

                  <div class="row">
                    <div class="col-12 d-flex justify-content-center">
                      {{ $posts->links() }}
                    </div>
                  </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
