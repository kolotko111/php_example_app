@extends('layouts.app')

@section('content')
  <div class="container">
    <h1>New Post</h1>
    <hr />
    <form method="post" action="{{ route('posts.update', $post->id) }}" id="update-post">
      {{ method_field('put') }}
      {{ csrf_field() }}
      <div class="form-group">
        <label for="title">Title</label>
        <input type="text" class="form-control" id="title" placeholder="Title" value="{{ $post->title }}" name="title" required>
      </div>

      <div class="form-group">
        <label for="content">Post Content</label>
        <textarea class="form-control" rows="8" id="content" placeholder="Write something amazing..." name="content" required>{{ $post->content }}</textarea>
      </div>

      <button type="submit" class="btn btn-primary btn-lg">Save Post</button>
    </form>

  </div>
@endsection
