<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;
use App\Post;
use App\User;

$factory->define(Post::class, function (Faker $faker) {
    return [
      'title' => $faker->name,
      'content' => $faker->paragraph,
      'user_id' => factory(User::class)->create()->id,
    ];
});
