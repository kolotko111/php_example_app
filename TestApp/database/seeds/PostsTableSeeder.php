<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Post;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $users = User::all();

      foreach($users as $user)
      {
        $user->posts()->create([
          'title' => 'Example title'. $user->id,
          'content' => 'Ut auctor ornare dolor sit amet auctor. Duis gravida dolor et eros aliquet dapibus. Ut tempus nibh et neque varius pulvinar. Phasellus sem arcu, pulvinar a sapien ut, aliquam varius elit. Mauris laoreet quis leo ut congue. Integer quis augue elit. Praesent quis blandit turpis. Duis eget ex egestas, vehicula orci id, sagittis augue. Vivamus lacinia purus sed consequat scelerisque.',
        ]);
      }
    }
}
