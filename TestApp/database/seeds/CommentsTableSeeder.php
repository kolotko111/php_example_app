<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Post;
use App\Comment;

class CommentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $users = User::all();
      $posts = Post::all();

      foreach($posts as $post)
      {
        foreach($users as $user)
        {
          $post->comments()->create([
            'content' => 'Random comment'. $user->name,
            'user_id' => $user->id,
          ]);
        }
      }
    }
}
