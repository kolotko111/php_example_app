<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Role;
use Illuminate\Support\Facades\DB ;
use Carbon\Carbon;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      User::truncate();
      Db::table('role_user') ->truncate();

      $admin_role = Role::where('name', 'admin')->first();
      $user_role = Role::where('name', 'user')->first();

      for ($i = 1; $i <= 5; $i++)
      {
        $admin = User::create([
          'name' => 'Admin' .$i. 'User',
          'email' => 'admin' .$i. '@admin' .$i. '.com',
          'password' => Hash::make('ZAQ!2wsx'),
          'avatar' => 'uploads/tEZtw86cy1w9WE8ndJm37gpGHnoncCFAGhEC7RUK.jpeg',
          'email_verified_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        $admin->roles()->attach($admin_role);
      }

      for ($i = 1; $i <= 50; $i++) {
        $user = User::create([
          'name' => 'User' .$i. 'User',
          'email' => 'user' .$i. '@user' .$i. '.com',
          'password' => Hash::make('ZAQ!2wsx'),
          'avatar' => 'uploads/tEZtw86cy1w9WE8ndJm37gpGHnoncCFAGhEC7RUK.jpeg',
          'email_verified_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        $user->roles()->attach($user_role);
      }
    }
}
