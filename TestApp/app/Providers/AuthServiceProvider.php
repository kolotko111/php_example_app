<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('admin-users', function($user){
        return $user->hasRole('admin');
      });

      Gate::define('edit-users', function($user){
        //the user can edit yourself
        $fullURL = \URL::full();
        $temp_string = substr($fullURL, 34);
        $user_id = substr($temp_string, 0, strpos($temp_string, "/edit"));

        if($user->id == $user_id)
        {
          //dd(false);
          return true;
        }

        //admin can edit too
        return $user->hasRole('admin');
      });

      Gate::define('update-post', function ($user, $post) {

        if($user->posts()->where('id', $post)->first())
        {
          return true;
        }

        //admin can edit too
        return $user->hasRole('admin');
      });
    }
}
