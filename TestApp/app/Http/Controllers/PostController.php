<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Post;
use App\User;

class PostController extends Controller
{
  public function index()
  {
    $posts = Post::paginate(20);
    return view('posts.index')->with('posts', $posts);;
  }

  public function search(Request $request)
  {
      $search_title = $request->get('search_title');

      $posts = Post::where('title', 'like', '%' .$search_title. '%')
                   ->paginate(20);

      return view('posts.index')->with('posts', $posts);
  }

  public function show($post_id)
  {
    $post = Post::findOrFail($post_id);
    return view('posts.show')->with('post', $post);
  }

  public function create()
  {
    return view('posts.create');
  }

  public function store(Request $request)
  {

    $this->validate($request, [
      'title' => 'required|max:255',
      'content' => 'required',
    ]);

    $user = Auth::user();



      $post = $user->posts()->create([
       'title' => $request->title,
       'content' => $request->content,
     ]);

    return redirect()->route('posts.show', $post->id);
  }


  public function edit($id)
  {
    $post = Post::findOrFail($id);
    return view('posts.edit')->withPost($post);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    $this->validate($request, [
      'title' => 'required|max:255',
      'content' => 'required',
    ]);

    $post = Post::findOrFail($id);
    $post->title = $request->title;
    $post->content = $request->content;
    $post->save();

    return redirect()->route('posts.show', $post->id);
  }

  public function destroy($id)
 {
   // $user->roles()->detach();
   // $user->delete();
   Post::destroy($id);
   return redirect()->route('posts.index');
 }
}
