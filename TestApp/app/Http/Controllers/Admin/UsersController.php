<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\User;
use App\Role;
use Gate;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class UsersController extends Controller
{
  public function __construct()
  {
    $this->middleware('auth');User::all();
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
      $users = User::paginate(20);
      return view('admin.users.index')->with('users', $users);
  }

  public function search(Request $request)
  {
      $search_name = $request->get('search_name');
      $search_email = $request->get('search_email');

      $users = User::where('name', 'like', '%' .$search_name. '%')
                   ->where('email', 'like', '%' .$search_email. '%')
                   ->paginate(20);

      return view('admin.users.index')->with('users', $users);
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  \App\User  $user
   * @return \Illuminate\Http\Response
   */
  public function edit(User $user)
  {
      if(Gate::denies('edit-users'))
      {
        return redirect(route('admin.users.index'));
      }

      $roles = Role::all();
      return view('admin.users.edit')->with([
        'user' => $user,
        'roles' => $roles,
      ]);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  \App\User  $user
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, User $user)
  {
      $data = request()->validate([
        'name' => 'required',
        'email' => 'required',
        'avatar' =>  'image|mimes:jpeg,png,jpg',
      ]);

      $user->roles()->sync($request->roles);

      $user->name = $data['name'];
      $user->email = $data['email'];

      if(!is_null($request['avatar']))
      {
        $avatar_path = $data['avatar']->store('uploads', 'public');
        $user->avatar = $avatar_path;
      }

      $user->save();
      return redirect()->route('admin.users.index');
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  \App\User  $user
   * @return \Illuminate\Http\Response
   */
  public function destroy(User $user)
  {
    if(Gate::denies('admin-users'))
    {
      return redirect(route('admin.users.index'));
    }

    $user->roles()->detach();
    $user->delete();

    return redirect()->route('admin.users.index');
  }
}
