<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Gate;
use Tests\TestCase;
use App\Role;
use App\User;

class GateAdminUserTest extends TestCase
{
  use RefreshDatabase;

  /** @test */
  public function a_user_with_correct_role()
  {
    //Ciekawostka, jeśli fałsz to jest ok, jeśli true to nie ma dostępu
      $admin_role = factory(Role::class)->create();
      $user = factory(User::class)->create();
      $user->roles()->attach($admin_role);
      $this->actingAs($user);
      $result = Gate::denies('admin-users');

      $this->assertFalse($result);
  }

  /** @test */
  public function a_user_with_wrong_role()
  {
      $user = factory(User::class)->create();
      $this->actingAs($user);
      $result = Gate::denies('admin-users');

      $this->assertTrue($result);
  }
}
