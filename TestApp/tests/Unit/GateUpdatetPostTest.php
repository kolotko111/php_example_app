<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Gate;
use Tests\TestCase;
use App\Role;
use App\User;
use App\Post;

class GateUpdatetPostTest extends TestCase
{
  use RefreshDatabase;
  /** @test */
  public function creator_can_update()
  {
      $user = factory(User::class)->create();
      $this->actingAs($user);
      $response = $this->post('/posts', [
        'title' => 'test title',
        'content' => 'Test content',
      ]);
      $post_id = Post::first()->id;
      $result = Gate::denies('update-post',$post_id);

      $this->assertFalse($result);
  }

  /** @test */
  public function admin_can_update()
  {
      $admin_role = factory(Role::class)->create();
      $user = factory(User::class)->create();
      $user->roles()->attach($admin_role);
      $this->actingAs($user);

      $post = factory(Post::class)->create();
      $post_id = $post->id;

      $result = Gate::denies('update-post',$post_id);

      $this->assertFalse($result);
  }

  /** @test */
  public function unauthorised_user_cant_update()
  {
      $post = factory(Post::class)->create();
      $post_id = $post->id;

      $result = Gate::denies('update-post',$post_id);

      $this->assertTrue($result);
  }
}
