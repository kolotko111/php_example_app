<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Post;
use App\User;

class PostManagementTest extends TestCase
{
  use RefreshDatabase;

  /** @test */
  public function an_post_create()
  {
    $user = factory(User::class)->create();
    $this->actingAs($user);
    $response = $this->post('/posts', [
      'title' => 'test title',
      'content' => 'Test content',
    ]);

    $posts = Post::all();
    $this->assertCount(1, $posts);

    $post = Post::first();


    $this->assertEquals('test title', $post->title);
    $this->assertEquals('Test content', $post->content);
  }

  /** @test */
  public function an_post_create_unauthorised_user()
  {
    $response = $this->post('/posts', [
      'title' => 'test title',
      'content' => 'Test content',
    ]);

    $posts = Post::all();
    $this->assertCount(0, $posts);

    $response->assertRedirect('login');
  }

  /** @test */
  public function a_title_is_required_create()
  {
    $user = factory(User::class)->create();
    $this->actingAs($user);
    $response = $this->post('/posts', [
      'title' => '',
      'content' => 'Test content',
    ]);

    $response->assertSessionHasErrors('title');
  }

  /** @test */
  public function a_content_is_required_create()
  {
    $user = factory(User::class)->create();
    $this->actingAs($user);
    $response = $this->post('/posts', [
      'title' => 'test title',
      'content' => '',
    ]);

    $response->assertSessionHasErrors('content');
  }

  /** @test */
  public function a_post_update()
  {
    $user = factory(User::class)->create();
    $this->actingAs($user);

    $response = $this->post('/posts', [
      'title' => 'test title',
      'content' => 'Test content',
    ]);

    $post_to_edit = Post::first();

    $response = $this->put('posts/' . $post_to_edit->id, [
      'title' => 'New title',
      'content' => 'New content',
    ]);

    $post_edited = Post::first();

    $this->assertEquals('New title', $post_edited->title);
    $this->assertEquals('New content', $post_edited->content);
  }

  /** @test */
  public function a_post_update_unauthorised_user()
  {
    $post_to_edit = factory(Post::class)->create();

    $response = $this->put('posts/' . $post_to_edit->id, [
      'title' => 'New title',
      'content' => 'New content',
    ]);
    $response->assertStatus(403);
  }

  /** @test */
  public function a_title_is_required_to_update()
  {
    $user = factory(User::class)->create();
    $this->actingAs($user);

    $response = $this->post('/posts', [
      'title' => 'test title',
      'content' => 'Test content',
    ]);

    $post_to_edit = Post::first();

    $response = $this->put('posts/' . $post_to_edit->id, [
      'title' => '',
      'content' => 'New content',
    ]);

    $response->assertSessionHasErrors('title');
  }

  /** @test */
  public function a_content_is_required_to_update()
  {
    $user = factory(User::class)->create();
    $this->actingAs($user);

    $response = $this->post('/posts', [
      'title' => 'test title',
      'content' => 'Test content',
    ]);

    $post_to_edit = Post::first();

    $response = $this->put('posts/' . $post_to_edit->id, [
      'title' => 'test title',
      'content' => '',
    ]);

    $response->assertSessionHasErrors('content');
  }

  /** @test */
  public function a_post_destroy()
  {
    $user = factory(User::class)->create();
    $this->actingAs($user);

    $response = $this->post('/posts', [
      'title' => 'test title',
      'content' => 'Test content',
    ]);

    $post_to_edit = Post::first();

    $response = $this->delete('posts/' . $post_to_edit->id);

    $this->assertCount(0, Post::all());
  }

  /** @test */
  public function a_post_destroy_unauthorised_user()
  {
    $post = factory(Post::class)->create();

    $response = $this->delete('posts/' . $post->id);

    $response->assertStatus(403);
  }
}
