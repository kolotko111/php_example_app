<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Role;
use App\User;

class AdminManagementTest extends TestCase
{
  use RefreshDatabase;
  //$this->withoutExceptionHandling();

  /** @test */
  public function a_user_can_be_update()
  {
    $admin_role = factory(Role::class)->create();
    $user_admin = factory(User::class)->create();
    $user_admin->roles()->attach($admin_role);
    $this->actingAs($user_admin);

    $user_to_edit = factory(User::class)->create();
    $this->assertCount(2, User::all());


    $response = $this->put('admin/users/' . $user_to_edit->id, [
      'name' => 'New Name',
      'email' => 'newemail@newemail.com',
    ]);


    $user_to_compare = User::where('id', '=', $user_to_edit->id)->first();


    $this->assertEquals('New Name', $user_to_compare->name);
    $this->assertEquals('newemail@newemail.com', $user_to_compare->email);
  }

  /** @test */
  public function a_user_can_be_deleted()
  {
    $admin_role = factory(Role::class)->create();
    $user_admin = factory(User::class)->create();
    $user_admin->roles()->attach($admin_role);
    $this->actingAs($user_admin);

    $user_to_delete = factory(User::class)->create();
    $this->assertCount(2, User::all());

    $response = $this->delete('admin/users/' . $user_to_delete->id);

    $this->assertCount(1, User::all());
  }

  /** @test */
  public function a_name_is_required()
  {
    $admin_role = factory(Role::class)->create();
    $user_admin = factory(User::class)->create();
    $user_admin->roles()->attach($admin_role);
    $this->actingAs($user_admin);

    $user_to_edit = factory(User::class)->create();
    $this->assertCount(2, User::all());

    $response = $this->put('admin/users/' . $user_to_edit->id, [
      'name' => '',
      'email' => 'newemail@newemail.com',
    ]);

    $response->assertSessionHasErrors('name');
  }

  /** @test */
  public function a_email_is_required()
  {
    $admin_role = factory(Role::class)->create();
    $user_admin = factory(User::class)->create();
    $user_admin->roles()->attach($admin_role);
    $this->actingAs($user_admin);

    $user_to_edit = factory(User::class)->create();
    $this->assertCount(2, User::all());

    $response = $this->put('admin/users/' . $user_to_edit->id, [
      'name' => 'New Name',
      'email' => '',
    ]);

    $response->assertSessionHasErrors('email');
  }
}
